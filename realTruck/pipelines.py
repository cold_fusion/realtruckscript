# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exporters import CsvItemExporter
from scrapy.utils.project import get_project_settings
import os
from datetime import datetime
class RealtruckPipeline(object):
    def process_item(self, item, spider):
        return item

class CsvPipeline(object):
    def __init__(self):
        List_Page = None
        Path_Url=get_project_settings().get('PATH_LIST_URL')
        with open(os.path.join(os.getcwd().replace('realTruck/spiders', ''),Path_Url)) as url_list:
           List_Page=[url.replace('\n','') for url in url_list.readlines()]

        splitted_url = List_Page[0].split("/")
        file_name = splitted_url[len(splitted_url)-2]+ ' ' + datetime.now().ctime().replace(':', '') + '.csv'
        self.file = open(file_name, 'wb')
        self.exporter = CsvItemExporter(self.file, str)
        self.exporter.start_exporting()

    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item
