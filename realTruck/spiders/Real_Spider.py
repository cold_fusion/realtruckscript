# -*- coding: utf-8 -*-
import scrapy
import json
import re
import os
import pandas
import glob
from lxml import etree, html
from io import StringIO
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from datetime import datetime
from scrapy.utils.project import get_project_settings


class RealSpiderSpider(scrapy.Spider):
    name = 'Real_Spider'
    allowed_domains = ['realtruck.com']
    start_urls = ['http://realtruck.com/']
    sr_num = 1
    # Parse List URLs and add to List_Pages
    Path_Url = get_project_settings().get('PATH_LIST_URL')
    with open(os.path.join(os.getcwd().replace('realTruck/spiders', ''), Path_Url)) as url_list:
        List_Pages = [url.replace('\n', '') for url in url_list.readlines()]

    # plid=2904&isFiltered=0&isPreview=0&d_ye=2011&d_ma=&d_mo=&d_bo=&d_a1=-1&onlyUniversals=&first_time=false&origin=d_ye

    # Item data
    Item_Keys=["category_ids","sr","type","Brand","name","attribute_set","configurable_attributes","sku","comma","original_sku","manufacturer","price","special_price","jobber_price","cost","cta_year","cta_make","cta_model","cta_body","cta_color","cta_option1","cta_option2","cta_option3","options_container","description","short_description","thumbnail","small_image","image","qty","is_in_stock","use_config_manage_stock","tax_class_id","has_options","required_options","visibility","weight","simples_skus","magmi:delete"]
    
    Item_Defaults={'comma':',','qty':'5000','is_in_stock':'1','use_config_manage_stock':'1','tax_class_id':'2','has_options':'0','required_options':'0','visibility':'1','weight':'100','magmi:delete':'1'}
    manufacturer = None
    Item={Key:'' for Key in Item_Keys}

    for key,value in Item_Defaults.items():
        Item[key]=value

    
    # Form Data
    formDict = {
            "plid":"2904",
            "isFiltered":"0",
            "isPreview":"0",
            "d_ye":"2011",
            "d_ma":"",
            "d_mo":"",
            "d_bo":"",
            "d_a1":"-1",
            "onlyUniversals":"",
            "first_time":"false",
            "origin":"d_ye"
            }


    # Request Headers for the service
    Header_Request = {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64…) Gecko/20100101 Firefox/60.0',
                    'Referer': 'www.realtruck.com'
                }

    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        # TODO save current glob of csv data and store it into the final file
        exit()
        pass

    def start_requests(self):
        for URL in self.List_Pages:
            yield scrapy.Request(url=URL, headers=self.Header_Request, callback=self.parse, meta={'URL': URL})


    def parse(self, response):
        # Extract the plid first
        data = re.search("plid=(.+?);", response.body.decode('utf-8'))
        PLID = data.group(1)
        
        # Extract all available model years
        data = response.xpath("//select[contains(@id, 'd_ye')]//option//@value").extract()
        years = data[1:]

        # Use a custom form variable
        formData = self.formDict


        # manufacturer
        self.manufacturer = response.xpath('//*[@id="product_image_carousel"]//div[2]//a//span[2]//meta//@content').extract()[0]

        #years = [str(i) for i in range(2018,2020)]


        for year in years:
            print(year)
            formData.update({"plid": PLID})
            formData.update({"d_ye": year})
            
            yield scrapy.FormRequest(url="https://www.realtruck.com/event/drillChanged/", formdata=formData, callback=self.parseMake, meta={'formData':formData.copy(), 'year':year, 'URL':response.meta['URL']})



        # TODO Extract all the available years for the product

    def parseMake(self, response):
        # Extract all makes for the given year
        makes = response.xpath("//option[not(contains(@data-text, '----'))]/@value").extract()
        makes = [make.strip("\\\"") for make in makes][1:]

        t_makes = response.xpath("//option[not(contains(@data-text, '----'))]/@data-text").extract()
        t_makes = [t_make.strip("\\\"") for t_make in t_makes][1:]

        #print(makes)
        #print("Run for make: "+str(len(makes)))

        formData = response.meta['formData']
        formData.update({"origin":"d_ma"})

        for make, t_make in zip(makes, t_makes):
            formData.update({"d_ma": make})
            yield scrapy.FormRequest(url="https://www.realtruck.com/event/drillChanged/", formdata=formData, callback=self.parseModel, meta={'formData':formData.copy(), 'year':response.meta['year'], 'make': t_make, 'make_option_id': make, 'URL':response.meta['URL']})


    def parseModel(self, response):
        # Extract all the models for the given make
        models = response.xpath("//option[not(contains(@data-text, '----'))]/@value").extract()
        models = [model.strip("\\\"") for model in models][1:]


        """
        # Broken implementation of t_models
        t_models = response.xpath("//option[not(contains(@data-text, '----'))]/@data-text").extract()
        t_models = [t_model for t_model in t_models][1:]

        """

        
        base_resp = response.body.decode('utf-8')
        data = json.loads(base_resp)

        data['Response'] = data['Response'].replace("\n", "")
        html = data['Response']
        
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(html), parser)

        t_models = tree.xpath("//option[not(contains(@data-text, '----'))]/@data-text")[1:]

        print(t_models)
        #print("Run for Models: "+str(len(models)))

        formData = response.meta['formData']
        formData.update({"origin":"d_mo"})

        for model, t_model in zip(models, t_models):
            formData.update({"d_mo": model})
            yield scrapy.FormRequest(url="https://www.realtruck.com/event/drillChanged/", formdata=formData, callback=self.parseBody, meta={'formData':formData.copy(),'URL':response.meta['URL'], 'year':response.meta['year'], 'make':response.meta['make'], 'make_option_id':response.meta['make_option_id'], 'model':t_model, 'model_option_id': model})

    def parseBody(self, response):

        # Extract all the body types for the given model
        body_types = response.xpath("//option[not(contains(@data-text, '----'))]/@value").extract()
        body_types = [body_type.strip("\\\"") for body_type in body_types][1:]

        t_body_types = response.xpath("//option[not(contains(@data-text, '----'))]/text()").extract()
        t_body_types = [t_body_type.strip("\\\"")  for t_body_type in t_body_types][1:]
        

        formData = response.meta['formData']
        formData.update({"origin":"d_bo"})
        print(body_types)
        print(t_body_types)
        for body_type, t_body_type in zip(body_types, t_body_types):
            print('body index')
            print(body_type)
            formData.update({"d_bo":body_type})
            yield scrapy.FormRequest(url="https://www.realtruck.com/event/drillChanged/", formdata=formData, callback=self.parseColor, meta={'formData':formData.copy(),'URL':response.meta['URL'],'year':response.meta['year'], 'make':response.meta['make'], 'make_option_id':response.meta['make_option_id'], 'model': response.meta['model'], 'model_option_id': response.meta['model_option_id'], 'body_option_id': body_type, 'body':t_body_type})


    def parseColor(self, response):

        # Extract the colors for all the products
        # TODO Extract colors
        #print("RESPONSE: \n"+response.body.decode("utf-8"))
        
        
        colors = response.xpath("//li//@data-value").extract()
        t_colors = response.xpath("//li//@data-text").extract()

        t_colors = [t_color.strip("\\\"") for t_color in t_colors]

        print("Colors: "+str(colors))

        formData = {m_key: response.meta['formData'][m_key] for m_key in ['plid','isFiltered','isPreview','d_ye', 'd_ma', 'd_mo', 'd_bo', 'd_a1', 'onlyUniversals']}

        new_URL = response.meta['URL']+"R18"+response.meta["formData"]["plid"]+"P"+formData["d_ye"] + "Y"+formData["d_mo"]+"MA.html"
        formData.update({"plid":str(response.meta["formData"]["plid"])})
        formData.update({"isFiltered":str(1)})
        formData.update({"isPreview":str(0)})


        
        formData.update({"d_bo":response.meta['body_option_id']})
        print("New URL:\n"+new_URL)
        # TODO send FormRequest
        for color, t_color in zip(colors, t_colors):
            print('color code')
            
            formData.update({"d_a1":color})
            formData.update({"first_time":"true"})
            formData.update({"origin":'d_a1'})
            print('formdata params')
            print(formData)
            response.meta['color_option_id'] = color
            response.meta['color'] = t_color
            

            yield scrapy.FormRequest(url=new_URL, method='POST', formdata=formData, callback=self.parseProducts, meta=response.meta)


    def parseProducts(self, response):
        formData = {
                'plid' : '',
                'isFiltered' : '',
                'isPreview' : '',
                'd_ye' : '',
                'd_ma' : '',
                'd_mo' : '',
                'd_bo' : '',
                'd_a1' : '',
                'onlyUniversals' : '',
                'first_time' : '',
                'groupId' : '',
                'd_a2' : '',
                'd_an' : '',
                'origin' : ''
            }

        # Extract the product info and store in CSV file
        print("[---EXTRACTING PRODUCT INFO---]")
        print('model_option_id')
        
        
        # TODO Extract product info from the table attr
        
        our_prices = response.xpath("//div[contains(@id, 'Order')]//div[contains(@class, 'active_price')]//span//text()").extract()
        prices = response.xpath("//div[contains(@id, 'Order')]//div[contains(@class, 'high_price')]//text()").extract()
        names = response.xpath("//div[contains(@id, 'Order')]//div[contains(@class, 'info_header')]//text()").extract()
        group_ids = response.xpath("//div[@id='Order']//form/@data-drill-value").extract()
        product_ids = response.xpath("//li[contains(@data-text, 'All')]/@data-value").extract()
        bodytext = response.xpath("//body").extract()
        f = open("bodycontent.txt","w+")
        f.write("body text %s \r\n"% bodytext)
        print("[----PRODUCT IDS----]\n"+str(product_ids))

        # part_numbers = response.xpath('//table//tbody//tr//td[2]//div[2]//div[6]//div[2]//a//text()').extract()
        make = response.meta["make"]
        model = response.meta["model"]
        year = response.meta["year"]
        color=response.meta["color"]
        body = response.meta["body"]

        model_option_id = response.meta['model_option_id']
        make_option_id = response.meta['make_option_id']
        body_option_id = response.meta['body_option_id']
        color_option_id = response.meta['color_option_id']
        

        sub_models_ids = []
        
        for group_id in group_ids:
            # //*[@id="group_180792_d_an"]/div[2]/ul/li[2]/@data-value
            print(group_id)
            sub_model_elem = response.xpath('//*[@id="group_'+group_id+'_d_an"]/div[2]/ul/li[1]/@data-value').extract()  
            print('sub model id')
            print(sub_model_elem[0])
            sub_models_ids.append(sub_model_elem[0])
        print('part numbers and group numbers')
        print(len(group_ids))
        print(sub_models_ids)
        body = response.meta["body"]
        for size in [our_prices, prices, names, group_ids, sub_models_ids]:
            print("SIZE OF DATA: "+str(len(size)))

        # TODO yield the item
        for name, price, our_price, group_id, p_id, sub_model_id in zip(names, prices, our_prices, group_ids, product_ids, sub_models_ids):
            item = self.Item
            print("[------------------Here BOI----------------------]")
            response.meta['price'] = price
            response.meta['special_price'] = our_price
            response.meta['name'] = name
            
            print(p_id)
            print('p_id')
            

            formData['plid'] = re.search('plid=(.*?);', response.body_as_unicode()).group(1)
            formData['isFiltered'] = str(1)
            formData['isPreview'] = str(0)
            formData['d_ye'] = year
            formData['d_ma'] = make_option_id
            formData['d_mo'] = model_option_id
            formData['d_bo'] = body_option_id
            formData['d_a1'] = color_option_id


            formData['onlyUniversals'] = ''
            formData['first_time'] = 'true'
            formData['groupId'] = "group_"+group_id
            formData['d_a2'] = group_id
            formData['d_an'] = sub_model_id
            formData['origin'] = 'd_an'
            
            # response.meta['Item'] = item.copy()
            print('print formdata')
            
            print(formData)
            response.meta['sr'] = self.sr_num
            yield scrapy.FormRequest(url="https://www.realtruck.com/event/drillChanged/", method='POST', formdata=formData, meta=response.meta, callback=self.parsePartID)
            self.sr_num += 1


    def parsePartID(self, response):
        api_res_html = json.loads(response.body_as_unicode()).get("Response")

        print('parse part id')
        parse_content = response.xpath("//body").extract()
        
        part_id = html.fromstring(api_res_html).xpath('//table/tr/td[2]/div[2]/div[6]/div[2]/a/text()')
        f = open("partidcontent.txt","w+")
        f.write("parsed text %s \r\n"% api_res_html)
        # print(api_res_html)
        print(part_id[0])
        
        item = self.Item
        item['sr'] = response.meta['sr']
        item['cta_make'] = response.meta['make']
        item['cta_model']= response.meta['model']
        item['cta_year']=response.meta['year']
        item['cta_color']=response.meta['color']
        item['price']=response.meta['price']
        item['cta_body']=response.meta['body'].split("    ")[4].split("\\n")[0]
        item['original_sku']=part_id[0]
        item['special_price']= response.meta['special_price']
        item['name']=response.meta['name']
        item['manufacturer']= self.manufacturer
        yield item
        # print('item details')
        # print(item)